# Alola!

This is a fairly raw dump of the
[pkmncards.com](https://pkmncards.com) database. It is not updated
automatically, nor is it the authoritative source for data on the
website: it is merely an export that I may or may not update
periodically.

Do not rely on it or expect it to be useful for tracking new card
additions or for new fixes to old cards.

## Reporting / Fixing Errata

If you use this data and find errors in it, please feel free to send a
pull request to point them out. We will *manually* relay the fixes
back to the production site. Consider this a convenient bug reporting
mechanism, and nothing more.

Note that not all errors visible on pkmncards.com may be reflected in
this data due to post-processing that may occur when pages are
rendered. If you find such an error, feel free to open an issue ticket
here *and* report it in the [pkmncards.com discord #development
channel](https://discord.gg/qdwRVvU).

## Reporting missing cards

If you'd like to report a missing card, please drop us a line in the
[pkmncards.com discord #curation channel](https://discord.gg/YwGcUba).

Any card that is not yet officially released will not be added to the
website. Some cards, like Energy cards and "variant" cards from
Trainer Kit decks that differ only in minor visual elements are (at
the moment) deliberately not posted to pkmncards.com in order to
prevent search results from becoming overwhelmed by nearly identical
cards.

(Put another way: The search is presently calibrated more for players
and less for collectors.)
